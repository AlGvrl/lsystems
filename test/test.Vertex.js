if (typeof module !== 'undefined' && module.exports) {
    var mocha = require ('mocha'),
    chai = require ('chai'),
    Vertex = require ('../src/Vertex.js');
}
var should = chai.should ();

describe ('Vertex', function () {

    describe ('Constructor', function () {
        it ('should properly initialize all fields', function () {
            var parentObj = {},
                vtx = new Vertex (1, 2, parentObj);
            vtx.x.should.equal (1);
            vtx.y.should.equal (2);
            vtx.parent.should.equal (parentObj);
            vtx.children.should.deep.equal ([]);
            vtx.color.should.equal ('333333');
            vtx.stepLength.should.equal (1);
            vtx.direction.should.equal (90);
        });

        it ('should properly initialize all fields when no parameters are passed', function () {
            var vtx = new Vertex ();
            vtx.x.should.equal (0);
            vtx.y.should.equal (0);
            vtx.parent.should.equal (vtx);
            vtx.children.should.deep.equal ([]);
            vtx.color.should.equal ('333333');
            vtx.stepLength.should.equal (1);
            vtx.direction.should.equal (90);
        });

        it ('should save parent\'s direction even if it is 0', function () {
            var vtx = new Vertex;
            vtx.direction = 0;
            child = vtx.createChild ();
            child.direction.should.equal (0);
        });
    });

    describe ('createChild', function () {
        it ('should save children in the array', function () {
            var vtx = new Vertex(),
                child1 = vtx.createChild ();
            vtx.children.should.deep.equal ([child1]);
            var child2 = vtx.createChild ();
            vtx.children.should.deep.equal ([child1, child2]);
        });

        it ('should create children with proper coordinates and parent', function () {
            var vtx = new Vertex;
            vtx.stepLength = 3;
            vtx.direction = 120;
            child = vtx.createChild ();
            child.x.should.equal (vtx.x + 3 * Math.cos (Math.PI * 2 / 3));
            child.y.should.equal (vtx.y + 3 * Math.sin (Math.PI * 2 / 3));
            child.parent.should.equal (vtx);

            child = vtx.createChild (2,3);
            child.x.should.equal(2);
            child.y.should.equal(3);
        });
    });

    describe ('addChild', function () {
        it ('should push new children into array', function () {
            var vtx = new Vertex (),
                child1 = new Vertex (),
                child2 = new Vertex ();
            vtx.addChild (child1);
            vtx.children.should.deep.equal([child1]);
            vtx.addChild (child2);
            vtx.children.should.deep.equal([child1,child2]);
        });
    });

    describe ('clone', function () {
        it ('should create a new object', function () {
            var vtx = new Vertex (),
                clone = vtx.clone();
            clone.should.not.equal(vtx);
        });

        it ('should create an object with the same properties', function () {
            var vtx = new Vertex (),
                clone = vtx.clone();
            clone.should.deep.equal(vtx);
        });
    });
});