if (typeof module !== 'undefined' && module.exports) {
    var mocha = require ('mocha'),
    chai = require ('chai'),
    LSystem = require ('../src/LSystem.js');
}
var should = chai.should ();

describe ('LSystem', function () {

    describe ('Constructor', function () {
        it ('should assign all passed values to the object', function () {
            var lsys = new LSystem (null, 'F+F', {a: 'bc', d: 'ef'}, 35);
            lsys.axiom.should.equal ('F+F');
            lsys.state.should.equal ('F+F');
            lsys.rules.should.deep.equal ({a : 'bc', d : 'ef'});
            lsys.get('angle').should.equal (35);
        });

        it ('should fall back to default values if nothing is passed', function () {
            var lsys = new LSystem ();
            lsys.axiom.should.equal ('');
            lsys.state.should.equal ('');
            lsys.rules.should.deep.equal ({});
            lsys.get('angle').should.equal (60);
        });
    });



    describe ('Settings', function () {
        it ('should be able to set and get values', function () {
            var lsys = new LSystem ();
            lsys.set ('foo', 'bar');
            lsys.set ('baz', 2);
            lsys.get('foo').should.equal('bar');
            lsys.get('baz').should.equal(2);
        });

        it ('should return undefined it the value is not set', function () {
            var lsys = new LSystem ();
            should.equal(lsys.get('foo'), undefined);
        });
    });



    describe ('String processing', function () {

        describe ('addRule', function () {
            it ('should save rule', function () {
                var lsys = new LSystem ();
                lsys.addRule ('a', 'abc');
                lsys.rules.should.deep.equal ({'a':'abc'});
            });
        });

        describe ('clearRules', function () {
            it ('should remove all existant rules', function () {
                var lsys = new LSystem ();
                lsys.addRule ('a', 'abc');
                lsys.addRule ('f', 'bar');
                lsys.clearRules ();
                lsys.rules.should.deep.equal ({});
            });
        });

        describe ('iterate', function () {
            it ('should correctly work with one rule and one iteration', function () {
                var lsys = new LSystem (null, 'aa');
                lsys.addRule ('a', 'abc');
                lsys.iterate (1);
                lsys.state.should.equal('abcabc');
            });

            it ('should correctly work with multiple rules applied at once', function () {
                var lsys = new LSystem (null, 'abc');
                lsys.addRule ('c', 'ca');
                lsys.addRule ('a', 'ab');
                lsys.addRule ('b', 'bc');
                lsys.iterate (1);
                lsys.state.should.equal('abbcca');
            });

            it ('should correctly work with recoursive rules', function () {
                var lsys = new LSystem (null, 'a');
                lsys.addRule ('a', 'aa');
                lsys.iterate (3);
                lsys.state.should.equal('aaaaaaaa');
            });

            it ('should continue iterating from current state', function () {
                var lsys = new LSystem (null, 'a');
                lsys.addRule ('a', 'aa');
                lsys.iterate (3);
                lsys.state.should.equal('aaaaaaaa');
                lsys.iterate (1);
                lsys.state.should.equal('aaaaaaaaaaaaaaaa');
            });
        });

        describe ('reiterate', function () {
            it ('should reset state before iterating', function () {
                var lsys = new LSystem (null, 'a');
                lsys.addRule ('a', 'aa');
                lsys.iterate (3);
                lsys.state.should.equal('aaaaaaaa');
                lsys.reiterate (1);
                lsys.state.should.equal('aa');
            });
        });

        describe ('buildGraph', function () {
            it ('should create a new vertex when moving forward', function () {
                var lsys = new LSystem (null, 'F');
                lsys.buildGraph ();
                child = lsys.graph.children [0];
                should.exist(child);
                child.parent.should.equal(lsys.graph);
            });

            it ('should change a vertex\'s directon when encounters + or -', function () {
                var lsys = new LSystem (null, '+F--');
                lsys.set('angle', 30)
                    .buildGraph ();
                var root = lsys.graph,
                    child = root.children[0];
                root.direction.should.equal (120);
                child.direction.should.equal (60);
            });

            it ('should be able to save and restore current vertex', function () {
                var lsys = new LSystem (null, '[+F][-F]');
                lsys.set('angle', 30)
                    .buildGraph ();
                var root = lsys.graph;
                root.children.length.should.equal (2);
                var child1 = root.children[0],
                    child2 = root.children[1];
                root.direction.should.equal (90);
                child1.direction.should.equal (120);
                child2.direction.should.equal (60);
            });

            it ('should be able to save and restore multiple vertices', function () {
                var lsys = new LSystem (null, '[+F[+F]]');
                lsys.set('angle', 30)
                    .buildGraph ();
                var root = lsys.graph,
                    child = root.children[0],
                    grandchild = child.children[0];
                root.direction.should.equal (90);
                child.direction.should.equal (120);
                grandchild.direction.should.equal (150);
            });
        });
    });
});