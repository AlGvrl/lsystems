(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var LS = require ('../src/extLSys.js');
var lsys = new LS.LSystem (document.getElementById('myCanvas'), 'F');
lsys.addRule ('F','++F-F-F-F-F-F+++F')
    .set ('angle', 60)
    .iterate (3)
    .buildGraph ()
    .draw ();
},{"../src/extLSys.js":4}],2:[function(require,module,exports){
(function () {

    'use strict';
    var Vertex;
    if (typeof module !== 'undefined' && module.exports) {
        Vertex = require ('./Vertex.js');
    } else {
        Vertex = window.Vertex;
    }

    function moveForward (vtx) {
        return vtx.createChild();
    }

    function turnCounterclockwise (vtx) {
        vtx.direction = (vtx.direction + this.get ('angle')) % 360;
        return vtx;
    }

    function turnClockwise (vtx) {
        vtx.direction = (vtx.direction + 360 - this.get ('angle')) % 360;
        return vtx;
    }

    function saveVertex (vtx) {
        this.get ('savedVertices').push({
            vertex : vtx,
            savedState : vtx.clone ()});
        return vtx;
    }

    function restoreVertex (vtx) {
        var vtxData = this.get ('savedVertices').pop();
        if (typeof vtxData === 'undefined') {
            return vtx;
        } else {
            var restoredVtx = vtxData.vertex,
            state = vtxData.savedState;
            for (var prop in state) {
                if (state.hasOwnProperty (prop)) {
                    restoredVtx[prop] = state[prop];
                }
            }
            return restoredVtx;
        }
    }

    function LSystem (canvas, axiom, rules, angle) {
        this.canvas = canvas;
        this.axiom = axiom || '';
        this.state = this.axiom;
        this.rules = rules || {};
        this.settings = {};
        this.settings.angle = angle || 60;
        this.settings.savedVertices = [];
        this.graph = new Vertex (0,0);
        this.bindings = {};
        this.bindSymbolToFunction ('F', moveForward);
        this.bindSymbolToFunction ('+', turnCounterclockwise);
        this.bindSymbolToFunction ('-', turnClockwise);
        this.bindSymbolToFunction ('[', saveVertex);
        this.bindSymbolToFunction (']', restoreVertex);
    }

    LSystem.prototype.get = function (prop) {
        return this.settings [prop];
    };

    LSystem.prototype.set = function (prop, value) {
        this.settings [prop] = value;
        return this;
    };

    LSystem.prototype.addRule = function (symbol, replacement) {
        this.rules [symbol] = replacement;
        return this;
    };

    LSystem.prototype.clearRules = function () {
        this.rules = {};
        return this;
    };

    LSystem.prototype.bindSymbolToFunction = function (symbol, func) {
        this.bindings [symbol] = func.bind(this);
        return this;
    };

    LSystem.prototype.copyBinding = function (newSymb, existingSymb) {
        if (this.bindings [existingSymb]) {
            this.bindings [newSymb] = this.bindings [existingSymb];
        }
        return this;
    };

    LSystem.prototype.clearBindings = function () {
        this.bindings = {};
        return this;
    };

    LSystem.prototype.iterate = function (n) {
        for (var i = 0; i < n; i++) {
            var newState = '';
            var l = this.state.length;
            for (var j = 0; j < l; j++) {
                if (this.state[j] in this.rules) {
                    newState += this.rules[this.state[j]];
                } else {
                    newState += this.state[j];
                }
            }
            this.state = newState;
        }
        return this;
    };

    LSystem.prototype.reiterate = function (n) {
        this.state = this.axiom;
        this.iterate(n);
        return this;
    };

    LSystem.prototype.buildGraph = function () {
        var l = this.state.length,
            currentVertex = this.graph;

        for (var j = 0; j < l; j++) {
            if (this.bindings[this.state[j]]) {
                currentVertex = this.bindings[this.state[j]] (currentVertex);
            }
        }
        return this;
    };

    LSystem.prototype.draw = function () {
        var ctx = this.canvas.getContext('2d'),
            vtxQueue = [this.graph],
            borders = findBorders (this.graph),
            scales = getScales(this.canvas.width, this.canvas.height, borders),
            globalScale = scales.scaleX < scales.scaleY ? scales.scaleX : scales.scaleY,
            vtx;

        ctx.clearRect (0, 0, this.canvas.width, this.canvas.height);

        while (vtx = vtxQueue.shift()) { //intentional '='
            for (var i = 0; i < vtx.children.length; i++) {
                vtxQueue.push (vtx.children[i]);

                var thisX = (vtx.x - borders.minX) * globalScale,
                    thisY = this.canvas.height - ((vtx.y - borders.minY) * globalScale),
                    childX = (vtx.children[i].x - borders.minX) * globalScale,
                    childY = this.canvas.height - ((vtx.children[i].y - borders.minY) * globalScale);
                ctx.strokeStyle = vtx.color;
                ctx.beginPath ();
                ctx.moveTo (Math.floor (thisX) + 0.5, Math.floor (thisY) + 0.5);
                ctx.lineTo (Math.floor (childX) + 0.5, Math.floor (childY) + 0.5);
                ctx.stroke ();
            }
        }
        return this;
    };

    function findBorders (rootVtx) {
        var vtxQueue = [rootVtx];
        var minX = Number.MAX_VALUE,
            maxX = -Number.MAX_VALUE,
            minY = Number.MAX_VALUE,
            maxY = -Number.MAX_VALUE,
            vtx;
        while (vtx = vtxQueue.shift()) { //intentional '='
            minX = vtx.x < minX ? vtx.x : minX;
            maxX = vtx.x > maxX ? vtx.x : maxX;
            minY = vtx.y < minY ? vtx.y : minY;
            maxY = vtx.y > maxY ? vtx.y : maxY;
            for (var i = 0; i < vtx.children.length; i++) {
                vtxQueue.push (vtx.children[i]);
            }
        }

        return {
            'minX' : minX,
            'maxX' : maxX,
            'minY' : minY,
            'maxY' : maxY
        };
    }

    function getScales (cnvWidth, cnvHeight, graphBorders) {
        var scaleX = Number.MAX_VALUE,
            scaleY = Number.MAX_VALUE;
        if (graphBorders.minX !== graphBorders.maxX) {
            scaleX = cnvWidth / (1.01 * (graphBorders.maxX - graphBorders.minX));
        }
        if (graphBorders.minY !== graphBorders.maxY) {
            scaleY = cnvHeight / (1.01 * (graphBorders.maxY - graphBorders.minY));
        }
        return {
            'scaleX' : scaleX,
            'scaleY' : scaleY
        };
    }

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = LSystem;
    } else {
        window.LSystem = LSystem;
    }
}());
},{"./Vertex.js":3}],3:[function(require,module,exports){
(function () {
    'use strict';
    function Vertex (x, y, parent) {
        this.x = x || 0;
        this.y = y || 0;
        this.parent = parent || this;
        this.children = [];
        this.color = '333333';
        this.stepLength = 1;
        if (parent) {
            this.direction = parent.direction;
        }
        if (typeof this.direction === 'undefined') {
            this.direction = 90; //degrees, starts from 'right' and goes ccw
        }
    }

    Vertex.prototype.createChild = function (x, y) {
        var child = new Vertex (
                x || this.x + this.stepLength * Math.cos (this.direction * Math.PI / 180),
                y || this.y + this.stepLength * Math.sin (this.direction * Math.PI / 180),
                this
            );
        this.children.push(child);
        return child;
    };

    Vertex.prototype.addChild = function (newChild) {
        this.children.push(newChild);
        return this;
    };

    Vertex.prototype.clone = function () {
        var cln = new Vertex();
        for (var prop in this) {
            if (this.hasOwnProperty (prop)) {
                cln[prop] = this[prop];
            }
        }
        return cln;
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = Vertex;
    } else {
        window.Vertex = Vertex;
    }

} ());
},{}],4:[function(require,module,exports){
exports.Vertex = require ('./Vertex.js'),
exports.LSystem = require ('./LSystem.js');
},{"./LSystem.js":2,"./Vertex.js":3}]},{},[1]);
