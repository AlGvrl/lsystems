var LS = require ('../src/extLSys.js');
var lsys = new LS.LSystem (document.getElementById('myCanvas'), 'F');
lsys.addRule ('F','++F-F-F-F-F-F+++F')
    .set ('angle', 60)
    .iterate (3)
    .buildGraph ()
    .draw ();