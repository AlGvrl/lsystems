(function () {
    'use strict';
    function Vertex (x, y, parent) {
        this.x = x || 0;
        this.y = y || 0;
        this.parent = parent || this;
        this.children = [];
        this.color = '333333';
        this.stepLength = 1;
        if (parent) {
            this.direction = parent.direction;
        }
        if (typeof this.direction === 'undefined') {
            this.direction = 90; //degrees, starts from 'right' and goes ccw
        }
    }

    Vertex.prototype.createChild = function (x, y) {
        var child = new Vertex (
                x || this.x + this.stepLength * Math.cos (this.direction * Math.PI / 180),
                y || this.y + this.stepLength * Math.sin (this.direction * Math.PI / 180),
                this
            );
        this.children.push(child);
        return child;
    };

    Vertex.prototype.addChild = function (newChild) {
        this.children.push(newChild);
        return this;
    };

    Vertex.prototype.clone = function () {
        var cln = new Vertex();
        for (var prop in this) {
            if (this.hasOwnProperty (prop)) {
                cln[prop] = this[prop];
            }
        }
        return cln;
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = Vertex;
    } else {
        window.Vertex = Vertex;
    }

} ());