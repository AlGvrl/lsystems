# L-systems #

A (rather old) implementation of [Lindenmayer systems](https://en.wikipedia.org/wiki/L-system) on canvas.

Just clone the repo and open any `*.html` from `examples/` directory. Try changing the canvas's width and height or number of iterations (`.iterate(x)`) to get better results. No fancy GUI, so just open the file in your editor of choice.